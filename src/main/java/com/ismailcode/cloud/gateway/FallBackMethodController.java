package com.ismailcode.cloud.gateway;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class FallBackMethodController {

    private static final String USER_SERVICE = "userService";
    private static final String DEPARTMENT_SERVICE = "departmentService";

    @GetMapping("/userServiceFallBack")
    @CircuitBreaker(name = USER_SERVICE, fallbackMethod = "userFallback")
    public String userServiceFallBackMethod() {
        return "User Service is taking longer than Expected" + "Please try again later";
    }

    @GetMapping("/departmentServiceFallBack")
    @CircuitBreaker(name = DEPARTMENT_SERVICE, fallbackMethod = "departmentFallback")
    public String departmentServiceFallBackMethod() {
        return "Department Service is taking longer than Expected" + "Please try again later";
    }
}
